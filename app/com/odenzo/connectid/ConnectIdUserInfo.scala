package com.odenzo.connectid

import com.typesafe.scalalogging.LazyLogging
import spray.json.JsObject

/**
 * Dont ask me, didn't think about yet. Hack eventually in cache'ed session
 * with that can be refreshed so we stay "stateless". But this implies access token in
 * encrypted session cookie. Not ideal but not toooo bad?
 * If round-trip with prompt = None works, maybe that is way to store refresh (via auth) in client sort-a
 *
 * @param connectid
 * @param name
 * @param phone
 * @param email
 */
case class ConnectIdUserInfo(
    connectid: Option[String] = None,
    name:      Option[String] = None,
    phone:     Option[String] = None,
    email:     Option[String] = None,
    lastJson:  String
) {
  def label: Option[String] = {
    Seq(name, phone, email, connectid).collectFirst { case Some(x) ⇒ x }
  }
}

trait OauthReplyParsing {

  /**
   * When we do a login we go get a bunch of data to get profile of the user.
   * TODO: Finalize a JSON library, back to json4s now? Store as JSON not string lastJson
   * @param json
   * @return
   */
  def fromOAuthLogin(json: JsObject): ConnectIdUserInfo = {
    import spray.json._
    json.fields.get("userinfo").map(yy ⇒ yy.asJsObject.fields)
      .fold {
        ConnectIdUserInfo(lastJson = json.toString())
      } { ui ⇒
        // Map the values to what I want and munge any names to what I want
        val fields = ui.mapValues {
          case JsString(s)   ⇒ s
          case JsBoolean(tf) ⇒ tf.toString
          case other         ⇒ "Not expecting anything but boolean and tf"

        }
        ConnectIdUserInfo(fields.get("sub"), fields.get("name"), fields.get("phone_number"), fields.get("email"),
          lastJson = json.toString())
      }
  }
}

object ConnectIdUserInfo extends LazyLogging with OauthReplyParsing {

}
