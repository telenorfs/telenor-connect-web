package com.odenzo.connectid

/** Check if this is being used or planned to be **/
case object CurrentUser {

}

trait ChannelUser
case object GuestUser extends ChannelUser
case class CurrentUser(name: String, phone: String, email: String, acctno: String, connectid: String, userid: String)
  extends ChannelUser
