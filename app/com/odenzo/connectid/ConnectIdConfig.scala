package com.odenzo.connectid

import com.typesafe.config.{ Config, ConfigFactory, ConfigRenderOptions }
import com.typesafe.scalalogging.LazyLogging
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._

/**
 * This contains static configuration via Typesafe Config for sub-path telenor.connect.auth
 * Loads application.conf then based on telenor.connect.auth.run_node it loads another file (defaults to dev)
 * This should be all the stuff related to ID Provider Side like Telenor.
 */
trait ConnectIdConfig extends LazyLogging {
  // trait mixed-in will actually execute many times as a reminder.

  val merged = {
    val root = ConfigFactory.load // Application config should load secret.conf if present. it does.
    val run_mode = root.getAs[String]("telenor.connect.auth.run_mode").getOrElse("dev")
    logger.warn(s"Run Mode [$run_mode] ")

    // logger.info(s"Root Config \n\n" + configToString(root) + "\n==============\n")

    // Now load the mode dependant config. This seems to work ok.
    val deplyConfigResource = s"application-$run_mode.conf"
    logger.warn(s"Deploy Environment Config Override:$deplyConfigResource")
    val run_mode_configs = ConfigFactory.load(deplyConfigResource)
    run_mode_configs.withFallback(root)
  }

  //logger.info(s"Merged Config \n\n" + configToString(merged) + "\n==============\n")

  // Like to set scaliform to align = for marked blocks
  val conf = merged.getConfig("telenor.connect.auth")
  val note = conf.getAs[String]("note").getOrElse("No Configuration File Note Given")
  val scope = conf.as[List[String]]("scope")
  val all_scopes = conf.as[List[String]]("scopes_all")
  val clientId = conf.as[String]("clientId")
  val idServer = conf.as[String]("server")
  // Full URL awaiting params
  val oauthCallback = conf.as[String]("oauthCallback")
  val ssoCallback = conf.as[String]("ssoCallback")
  val logoutCallback = conf.as[String]("logoutCallback")

  logger.info(s"Using Client ID [$clientId]")
  logger.info(s"Using Telenor Server [$idServer]")
  logger.info(s"""All Scopes ${scope.mkString("\n\t")}""")

  val gateway = merged.as[String]("telenor.connect.gateway.server")

  protected def configToString(c: Config): String = {
    val renderOpts = ConfigRenderOptions.defaults().setOriginComments(false).setComments(true).setJson(false)
    c.root().render(renderOpts)
  }
}

object ConnectIdConfig extends ConnectIdConfig
