package com.odenzo.connectid

import akka.http.scaladsl.model.Uri
import com.typesafe.scalalogging.LazyLogging
import play.api.mvc.Call

object ConnectIdRedirectBuilder extends LazyLogging with ConnectIdConfig {

  // ToDo: Load these from config and document them beter.
  // Or at least a see me to docs.telenordigital.com place.
  val standardParams = Map(
    "response_type" → "code", // No Need to change this
    "client_id" → clientId, // Issues by Telenor Digital
    "redirect_uri" → oauthCallback, // Where redirect redirects back too. Based on env and SSO check or real login

    // All the rest optional
    // "scope" → scope.mkString(" ") // These are the OAuth grant scope we ask for
    "scope" → "profile"
  )

  // This is stray thing I am not using now?
  "acr_values" → "2 1"

  // This is the allowed login levels. WTF it is doing here now I forget.

  /**
   * This hold the request query parameter information for us redirecting to Telenor Connect ID server
   * Note that some of these are custom values, or custom interpretations
   *
   * @param map
   */
  case class RedirectParams(map: Map[String, String] = standardParams) {

    def withState(state: String) = this.copy(map + ("state" → state))

    def withPassword = this.copy(map + ("acr_values" → "2"))

    def withForceLogin = this.copy(map + ("max_age" → "0"))

    /**
     * You really MUST set this and compare with the state returned on redirect
     */
    def withScopes(scopes: Seq[String]) = this.copy(map + ("scope" → scope.mkString(" ")))

    def withNoPrompt = this.copy(map + ("prompt" → "none"))

    def withConfirm = this.copy(map + ("prompt" → "confirm"))

    def withLogin = this.copy(map + ("prompt" → "login"))

    def withCallback(nCallback: String) = this.copy(map + ("redirect_uri" → nCallback))

    def withSSOCallback = this.withCallback(ssoCallback)

    def withOathCallback = this.withCallback(oauthCallback)

    def asUriQuery = Uri.Query(map)

    def dump: String = {
      map.toSeq.mkString("Redirect Params: ", "\n\t", "\n<<<<<<<")
    }
  }

  val uri = Uri(idServer)

  /**
   * This builds a full URL given the redirect params.  Not sure why I convert to String
   */
  protected def buildUrl(params: RedirectParams) = uri.withQuery(params.asUriQuery).toString()

  def ssoParams = RedirectParams().withSSOCallback

  def loginParams = RedirectParams().withOathCallback.withLogin

  /**
   *
   * @param params You are expected to set this up completely.
   * @return
   */
  def userAuthorizationRequest(params: RedirectParams) = {
    val params = RedirectParams()
    Call("GET", idServer, params.toString)
  }

  def ssoCheckRequest(st: String) = {
    val params = RedirectParams().withNoPrompt.withState(st).withSSOCallback
    val uri = idServer
    logger.info(s"URI $uri with params $params")
    val url = Uri(idServer).withQuery(Uri.Query(params.map))
    logger.info("Real URI to Go " + url.toString)
    Call("GET", url.toString)

  }

  protected def params2RedirectUrl(params: RedirectParams) = uri.withQuery(Uri.Query(params.map)).toString()

}

