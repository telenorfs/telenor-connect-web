package models

/**
 * Play frowns on having server side state, normally stuff is stored in database or cache to allow
 * full HA config without having to deal with sticky sessions etc.
 *
 * This is limited to 4K, but since it is client side adds to network traffic, so should be kept small
 * and only with data that cannot be reconstructured. Reconstructable data should be tossed in the cache
 *
 * Do this by extendinf RequestWrapper and use the standard implicit request => appraoch per PlayFrameowrk Book
 * Need to think about how to model in the real world.
 *
 */

trait AppSessionToken {
  val loggedIn: LoginLevel
  val name: String
}

case object GuestSessionToken extends AppSessionToken {
  val loggedIn = NotLoggedIn
  val name = "Guest" // TODO: Localized on display

}

case class UserSessionToken(loggedIn: LoginLevel, account: String, connectid: Option[String], userid: Option[String], name: String) extends AppSessionToken

case object UserSessionToken {

  // JSON Marshall/Unmarshell here. User to marshall into Play Session format which is encrypted header/cookie

}
