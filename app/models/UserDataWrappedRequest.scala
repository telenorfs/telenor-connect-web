package models

import play.api.mvc.{ WrappedRequest, Request }

// From the PlayFramework Book. implicit Request => changes to
case class UserDataWrappedRequest[A](user: AppSessionToken, request: Request[A]) extends WrappedRequest(request) {

}
