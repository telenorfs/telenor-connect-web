package models

sealed trait LoginLevel
case object NotLoggedIn extends LoginLevel
case object ConnectSSO extends LoginLevel
case object LocalSSO extends LoginLevel
case object ConnectL2 extends LoginLevel
case object LocalL2 extends LoginLevel

