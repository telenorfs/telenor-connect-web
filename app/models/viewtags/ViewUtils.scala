package models.viewtags

import play.mvc.Http.{ Session ⇒ JSession }
import play.api.mvc.Session

object ViewUtils {

  def asScala(jSession: JSession): Session = {
    import scala.collection.JavaConverters._
    val sessionMap = jSession.asScala.toMap // toMap to make immutable, better way?
    new Session(sessionMap)
  }

  def dumpMap(m: Map[Any, Any]) = {

  }
}
