package controllers

import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit._
import javax.inject.Inject

import com.odenzo.connectid.ConnectIdConfig
import com.odenzo.connectid.ConnectIdRedirectBuilder
import com.odenzo.connectid.ConnectIdUserInfo
import play.api.Logger
import play.api.cache.CacheApi
import play.api.libs.ws._
import play.api.mvc._
import spray.json.JsValue
import spray.json.JsonParser

import scala.concurrent.Future
import scala.concurrent.duration.Duration

case class OuthLoginCorrelator(loginType: String, sessionState: String)

class ConnectID @Inject() (ws: WSClient, cache: CacheApi) extends Controller {

  /**
   * This is the back-end call to gateway server component that takes care of talking
   * to Telenor Connect ID Server.
   *
   * Essentially, to keep stuff out of web tier we have a micro-service server running in data-center
   * (not in presentation layer). This does a REST style call down to it to query more information
   * from Telenor Connect ID server.
   * TODO: Excecution context should be implicit parameters passed in.
   *
   * @param token
   * @return
   */
  private def handleAuthToken(token: String): Future[JsValue] = {
    import scala.concurrent.ExecutionContext.Implicits.global

    Logger.info(s"SECURITY resolve_auth_token function at ${ConnectIdConfig.gateway}  $token")
    val request = ws.url(s"${ConnectIdConfig.gateway}/client/resolve_auth_token")
      .withHeaders("Accept" → "application/json")
      .withRequestTimeout(Duration(30, TimeUnit.SECONDS)) // To wait for Heroku spinup, 30 sec
      .withQueryString("code" → token)
      .withAuth("appLevel", "password", WSAuthScheme.BASIC) // TODO: Oauth or something later

    val fut: Future[WSResponse] = request.get()
    //cache.set("item.key", connectedUser)
    fut.map {
      case rs if rs.status == 200 ⇒
        val json = JsonParser(rs.body)
        Logger.debug("Reponse from client/resolve_auth_token:\n " + json.prettyPrint)
        json
      case apperr ⇒ // Unlikely, will probably throw exceptions on most bad cases already
        throw new IllegalStateException("Invalid Response From Gateway -- Check Logs")
    }

  }

  def logout = Action { implicit request ⇒
    // Kill the current user from the cache and drop session
    // Optionally could redirect through Connect ID which will do SSO logout and redirect back.
    Redirect(routes.Application.home()).withNewSession.flashing("success" → "You have been logged Out.")

    // /oauth/logout{?client_id,post_logout_redirect_uri,state,ui_locales}
    // This is used to logout from all SSO sessions
    // ot particularly applicable to header injection style SSO.

  }

  /**
   * This is what happens when a user clicks a button saying that want to login with connect id.
   * It sends them to a login page at the Identity Provider (Telenor Connect ID Page here)
   * Moved the SSO to Application, probably should do for this too
   *
   * @return
   */

  def doConnectIDLogin(level: Int = 1, force: Boolean = false) = Action { implicit request ⇒
    val state = request.session("state")
    val correlatorKey = UUID.randomUUID().toString
    val correlator = OuthLoginCorrelator("SSO", state)
    cache.set(correlatorKey, correlator, Duration(5, MINUTES))

    val params = ConnectIdRedirectBuilder.RedirectParams().withState(correlatorKey)
    val url = ConnectIdRedirectBuilder.userAuthorizationRequest(params)
    Logger.info("Redirecting to Connect ID with: " + url)
    Redirect(url)
  }

  /**
   * The doConnectIDLogin redirect should redirect back to here. Possible they will abort.
   * Best practice would be to do in a pop-up window I guess.
   * Note that this returns a Future result, since it is going to call Connect ID server to get more information
   * in the case the user is already logged in. May really want to do that async
   */
  def connectIdCallback() = Action { implicit request ⇒

    val byFlash = request.flash.get("login_style")

    val byState = request.getQueryString("state")

    val result = if (byFlash.contains("SSO")) {
      Logger.debug("Doing SSO Style via byFlash")
      ssoCallbackHandler(request)
    } else {
      Logger.debug("Doing Login Style via byFlash")
      loginCallbackHandler(request)
    }
    //result.get
    Ok(views.html.home())
  }

  def connectLoginCallback() = Action { implicit request ⇒

    Logger.info("Connect ID Callback Invoked..." + request)

    Ok(views.html.home())
  }

  def connectSsoCallback() = Action { implicit request ⇒

    Logger.info("Connect ID Callback Invoked..." + request)

    // In our specific use case, if SSO fails or login fails we go back to main welcome screen with flashing...

    Ok(views.html.home())
  }

  /**
   * For SSO login we don't care if the "level 0" login fails or not. Just mark no info to show.
   * @param req
   * @return
   */
  def ssoCallbackHandler(implicit req: Request[AnyContent]): Any = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val successData = for {
      loginInfo ← extractLogins(req)
    } yield {
      Logger.info("Extracted Logins: " + loginInfo)
      fetchServerInfo(loginInfo._1).map { userInfo ⇒
        // Stuff it somewhere amd go home.
        val cacheCheck = cache.get[OuthLoginCorrelator](loginInfo._1)
        Logger.info("Internal Cache Check Set Before Redirect: " + cacheCheck)
        cache.set(req.session("state") + "_sso", userInfo, Duration(30, TimeUnit.MINUTES))
        // cache.remove(req.getQueryString("state").get) // Wait on this for debugging porpoises
        Ok(views.html.home()(req)).addingToSession("SSO" → "true")
      }.recover {
        case e: Exception ⇒
          // cache.remove(req.getQueryString("state").get) // Wait on this for debugging porpoises
          Ok(views.html.home()).addingToSession("SSO" → "false")
      }

    }
    Logger.debug("SSO Result: " + successData)
    successData
  }

  /**
   * For a real login we need to do somethign special if login fails. Pending.
   * @param req
   * @return
   */
  def loginCallbackHandler(implicit req: Request[AnyContent]) = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val successData = for {
      loginInfo ← extractLogins(req)
    } yield {

      fetchServerInfo(loginInfo._1).map { userInfo ⇒
        // Stuff it somewhere amd go home.
        cache.set(req.session("state") + "_user", userInfo, Duration(30, TimeUnit.MINUTES))
        // cache.remove(req.getQueryString("state").get) // Wait on this for debugging porpoises
        Ok(views.html.home()(req)).addingToSession("LOGIN" → "true")
      }.recover {
        case e: Exception ⇒
          // cache.remove(req.getQueryString("state").get) // Wait on this for debugging porpoises
          Ok(views.html.home()).addingToSession("LOGIN" → "false")
      }

    }
    successData
  }

  /**
   *
   * @param request The redirect request back from Telenor Server
   * @return (session state variable contents and correlator.loginType)
   */
  protected def extractLogins(request: Request[AnyContent]): Option[(String, String)] = {
    val res = for {
      code ← request.getQueryString("code")
      state ← request.getQueryString("state")
      debug = Logger.info(s"Code $code and State $state")
      correlator ← cache.get[OuthLoginCorrelator](state) // Check the server cache for the request correlator
      _ = Logger.info(s"Correlator for State Param $correlator")
      if state == correlator.sessionState // Like to log an error in addition to just gaurding.
    } yield {
      Logger.info(s"Got the valid state $state and code $code")
      (state, correlator.loginType)
    }
    res
  }

  /**
   * Few cases here, if there is error then definately not logged in now.
   * But could be an SSO check and not an error per se.
   *
   * @param  request from the Connect ID redirect back to our website.
   * @return If no error information in query string then None, else Either with right false (not
   *         logged in) or Left details of an actual error case.
   */
  protected def extractErrors(request: Request[AnyContent]): Option[(String, String)] = {
    for {
      error ← request.getQueryString("error")
      errMsg ← request.getQueryString("error_description")
    } yield (error, errMsg)

  }

  protected def fetchServerInfo(authToken: String): Future[ConnectIdUserInfo] = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val answer = handleAuthToken(authToken).map { json ⇒

      Logger.info(s"Got JSON from Gateway Call $json")
      ConnectIdUserInfo.fromOAuthLogin(json.asJsObject)
    }
    answer
  }

}

