package controllers

import java.util.{ Date, UUID }

import com.typesafe.scalalogging.StrictLogging
import play.api.mvc.Session

trait SessionUtils extends StrictLogging {
  def ensureStateInSession: PartialFunction[Session, Session] = {
    case x if x.get("state").isEmpty ⇒ x + ("state" → UUID.randomUUID().toString)
  }

  def updateIfNotPresent(s: Session, entry: (String, String)): Session = {
    if (s.get(entry._1).isDefined) s
    else s + entry
  }

  def ensureStatePresent(s: Session): Session = {
    if (s.get("state").isDefined) s
    else {
      val uuid = UUID.randomUUID().toString
      logger.info(s"Setting new state UUID: $uuid")
      val ns = s + ("state" → uuid)
      logger.info("New Session: " + ns.data)
      ns
    }
  }

  def ensureStateTimePresent(s: Session): Session = {
    if (s.get("start").isDefined) s
    else s + ("start" → new Date().getTime.toString)
  }

  //def ensureBasicSession = ensureStatePresent compose ensureStateTimePresent

}
