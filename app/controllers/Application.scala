package controllers

import java.util.UUID
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import controllers.actions._
import com.odenzo.connectid.ConnectIdConfig
import com.odenzo.connectid.ConnectIdRedirectBuilder

import play.api.Logger
import play.api.cache.CacheApi

import play.api.mvc._

import scala.concurrent.duration.Duration

/**
 * This is simular to authentication action, but it allows a Guest.
 * We try and extract the current logged in user.
 * Also, if there is no session state I want to automagically create it, not sure if doable.
 * This action should really be used everywhere.
 */
//object EnhancedAction extends ActionBuilder[ UserDataWrappedRequest ] {
//
//  // TODO: Working on this
////  def invokeBlock[ A ](request: Request[ A ], block: Request[ A ] ⇒ Future[ Result ]): Future[ Result ] = {
////    Logger.debug(s"HTTP request: ${request.method} ${request.uri}")
////    val state = request.session.get("state")
////
////    block(new UserDataWrappedRequest(GuestSessionToken, request))
////  }
//}

class Application @Inject() (cache: CacheApi) extends Controller with SessionUtils {

  Logger.info(s"Triggering Config: Gateway =  ${ConnectIdConfig.gateway}")

  /**
   * Well, home is where the heart is...
   * This will do SSO check and move on to Welcome on the redirect.
   * So a redirect SSO check on every visit to https://mysite/
   * Add a filter so if no session variable on all other pages then redirect to here to force it
   * This will remove deep links into site for better or worse.
   *
   * @return
   */
  def home = SessionAction { implicit request ⇒
    logger.info("Current Session at Home: " + request.session.data)
    Ok(views.html.home())
  }

  def welcome = Action { implicit request ⇒
    val vsession = ensureStatePresent(request.session)
    logger.debug("Welcome State Session: " + vsession.data)
    logger.debug("Should be doing SSO redirect to Telenor Connect now..")

    // Note that normally you would check to see if they had a Paysbuy cookie for previous login etc. to id
    // return visitors.

    // SSO Round-trip on the assumption that cookie doesn't exist.

    // First store a correlator to session uuid/state in the cache with type of OAuth login request
    // This will use a seperate callback later btw.
    val state = vsession("state")
    val correlatorKey = UUID.randomUUID().toString
    val correlator = OuthLoginCorrelator("SSO", state)
    cache.set(correlatorKey, correlator, Duration(2, TimeUnit.MINUTES))
    Logger.debug("Correlator: " + correlator + " with Key " + correlatorKey)
    val url = ConnectIdRedirectBuilder.ssoCheckRequest(correlatorKey)
    Logger.info(s"Doing an SSO Check with hacked state: $url")
    Redirect(url.toString).withSession(vsession).flashing(("login_style", "SSO"))
    // Redirect to ID Provider who redirects back to us.
    // See
  }

  def about = SessionAction { implicit request ⇒ Ok(views.html.about()(request)) }

  def contact = SessionAction { implicit request ⇒ Ok(views.html.contact()(request)) }

  /**
   * Opens the Login Page, which will allow the user to choose an Open ID style login or
   * enter their local username and password. At this time, we don't know the account or anything,
   * because we haven't autodetected if a visitor is already logged into Telenor CONNECT
   *
   * @return
   */
  def login = SessionAction { implicit request ⇒

    Ok(views.html.login.login(None))
  }

  /**
   * This will handle regular and connect id logouts eventually.
   *
   * @return
   */
  def logout = Action { implicit request ⇒
    Logger.warn(s"Logging User Out")
    Ok(views.html.login.loggedOut(None)).withNewSession
  }

}
