package controllers.actions

import play.api.Logger
import play.api.mvc._

import scala.concurrent.Future

/**
 * A Play Action that ensures that the user has a current active session.
 * If not, then it sends them to the https://website/ page.
 * This is attempt to do in Composing Action style
 */
class SessionActionDef[A](action: Action[A]) extends Action[A] {
  def apply(request: Request[A]): Future[Result] = {
    Logger.info("SessionActionDef Invoked: " + request.session)
    action(request)
  }

  lazy val parser = action.parser

}

/**
 * A Play Action that ensures that the user has a current active session.
 * If not, then it sends them to the https://website/ page.
 */
object SessionAction extends ActionBuilder[Request] {
  override def invokeBlock[A](request: Request[A], block: (Request[A]) ⇒ Future[Result]): Future[Result] = {
    Logger.info("SessionAction invokBlock: " + request.session)
    if (request.session.get("state").isEmpty) {
      Logger.info("No Session so redirecting to / manually, not via routes.")
      Future.successful(Results.Redirect("/"))
    } else block(request)
  }

  override def composeAction[A](action: Action[A]) = new SessionActionDef[A](action)
}

//
//object HttpsOnly {
//def onlyHttps[A](action: Action[A]) = Action.async(action.parser) { request =>
//  request.headers.get("X-Forwarded-Proto").collect {
//    case "https" => action(request)
//  } getOrElse {
//    Future.successful(Forbidden("Only HTTPS requests allowed"))
//  }
//}
