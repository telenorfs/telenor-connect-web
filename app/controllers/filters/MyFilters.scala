package controllers.filters
import javax.inject.Inject

import play.api.http.HttpFilters
import play.filters.gzip.GzipFilter
//
// Need to  set   play.http.filters = "filters.MyFilters"
// e..g play.http.filters ="controllers.filters.Filters"

//class Filters @Inject() (gzipFilter: GzipFilter) extends HttpFilters {
//  def filters = Seq(gzipFilter)
//}
//
//class Filters @Inject() (securityHeadersFilter: SecurityHeadersFilter) extends HttpFilters {
//  def filters = Seq(securityHeadersFilter)
//}
//
//new GzipFilter(shouldGzip = (request, response) =>
//  response.headers.get("Content-Type").exists(_.startsWith("text/html")))
//
//import play.api.http.HttpFilters
//import play.filters.cors.CORSFilter
//
//class Filters @Inject() (corsFilter: CORSFilter) extends HttpFilters {
//  def filters = Seq(corsFilter)
//}
