package controllers

import play.api.mvc.{ WrappedRequest, Request, AnyContent, Session }

case class MyContext(nSession: Session, request: Request[AnyContent]) extends WrappedRequest(request)

