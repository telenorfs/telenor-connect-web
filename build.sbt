name := "telenor-connect-web"

version := "1.0.2-SNAPSHOT"


scalaVersion := "2.11.8"

// We require JDK 1.8 to show usage of the latest JSSE APIs.
initialize := {
  val _ = initialize.value // run the previous initialization
  val specVersion = sys.props("java.specification.version")
  assert(specVersion.contains("1.8"), "Java 1.8.0 or above required")
}

// See plugin version in project/plugins.sbt for versions of Play
// Play > 2.4.6 now
lazy val root = (project in file(".")).enablePlugins(PlayScala)

routesGenerator := InjectedRoutesGenerator

// Crappy Java Context Need to add import for explicit Scala conversions too?
TwirlKeys.templateImports +=  "play.mvc.Http.Context.Implicit._"


resolvers ++= Seq(
  "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/",
  "SonaType OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Scala Tools Releases" at "http://scala-tools.org/repo-releases/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/"
 // "Spray repository" at "http://repo.spray.io"
)



libraryDependencies ++= Seq(cache, filters, ws)
libraryDependencies ++= Seq(
 // "org.webjars" % "jquery" % "1.11.3",
  "org.webjars" % "bootstrap" % "3.3.6"

)

// -------- Back-End Style Dependancies on top of Play



libraryDependencies ++= {
  val akkaVersion = "2.4.2"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-agent" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-kernel" % akkaVersion withSources() withJavadoc(),
    //"com.typesafe.akka" %% "akka-persistence-experimental" % "2.3.12" withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion withSources() withJavadoc()
  )
}

/** JWT Stuff For Authentication/Authorization: https://bitbucket.org/b_c/jose4j/wiki/Home */
libraryDependencies += "org.bitbucket.b_c" % "jose4j" % "0.5.0"

libraryDependencies ++= {
  // Note: %% fails for me, have to do _2.11
  val akkaVersion = "2.4.2"
  Seq(
    "com.typesafe.akka" % "akka-stream_2.11" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" % "akka-http-core_2.11" % akkaVersion withSources() withJavadoc(),
    // "com.typesafe.akka" % "akka-http_2.11" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" % "akka-http-xml-experimental_2.11" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % akkaVersion withSources() withJavadoc(),
    "com.typesafe.akka" % "akka-http-testkit_2.11" % akkaVersion withSources() withJavadoc()
  )
}



// Based on standard Play build template from odenzo
libraryDependencies ++= Seq(
  // "org.scalaz" %% "scalaz-core" % "7.1.2" withSources(),
  "net.ceedubs" %% "ficus" % "1.1.2" withSources(),
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0" withSources()

  //   Instrument with Kamon too:    http://kamon.io/integrations/web-and-http-toolkits/spray/
  //"com.storm-enroute" %% "scalameter-core" % "0.6" withSources(),

)

libraryDependencies ++= {
  Seq(
    "org.scalacheck" %% "scalacheck" % "1.13.0" % "test" withSources() withJavadoc(),
    "org.scalatest" % "scalatest_2.11" % "2.2.6" % "test" withSources() withJavadoc()
  )
}



//
//libraryDependencies += "com.softwaremill" %% "akka-http-session" % "0.1.4"

/** Some Ditties to Speed up Compile / Run **/
//sources in(Compile, doc) := Seq.empty

publishArtifact in(Compile, packageDoc) := false



unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")


/** Development Testing and Deployment Stuff **/

// Below in docs it statess should work, but error here?
//sbt  run -Dhttp.port=8081
//devSettings := Map("play.server.http.port" -> "8080")



herokuJdkVersion in Compile := "1.8"

herokuAppName in Compile := "aqueous-peak-2189"

herokuProcessTypes in Compile := Map(
  "web" -> "target/universal/stage/bin/telenor-connect-web -Dhttp.port=$PORT"
  //"worker" -> "java -jar target/universal/stage/lib/my-worker.jar"
)


herokuConfigVars in Compile := Map(
  "MY_VAR" -> "some value",
  "JAVA_OPTS" -> "-XX:+UseCompressedOops"
)

// The fork and run auto-reload ain't working well for me.
//fork in run := true


fork in run := true
