logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.0")

// https://github.com/sbt/sbt-jshint
addSbtPlugin("com.typesafe.sbt" % "sbt-jshint" % "1.0.3")

//addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.1")

addSbtPlugin("com.heroku" % "sbt-heroku" % "0.5.3")


//https://github.com/rtimush/sbt-updates
// sbt dependencyUpdates  shows which libraries have updates available on Maven/Ivy
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.1.10")

// https://github.com/jrudolph/sbt-dependency-graph
// Invoke with:
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

// Only needed if no Play sbt-plugin ?
// addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.7.6")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.4.0")
