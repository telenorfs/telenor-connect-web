# What it is?

This is a simple Scala/Play and ScalaJS application that can be deployed on Heroku, and maybe later AWS.

In essence, it allows identification via Connect ID and also grant authorization.

It handles redirecting to ConnectID and the redirect from ConnectID on login.

It is a trash/throw-away program.

# Configuration and Deployment

* JDK 8  on OS X used, with IntelliJ 16 EAP IDE -- everything should be SBT deployable though.

* You will need to install Heroku Toolbelt - https://toolbelt.heroku.com/ 
     
* Heroku ID SteveF at the standard company
     

## Local Dev Running

* First ensure the telenor-connect-gateway is running on port 8082

* Current Telenor Callbacks include http://localhost:8081/...  to setup this do the following:
    * export ODENZO_RUN_MODE=dev    (other options are staging and prod)
    * The above will flip the config files.
    * Moving to just override the main conf to application-dev.conf in addition, now fancy loading
    * sbt "run 8081"     


Meh, not sure I have a 8443 port redirect or anything, so must run on 8081 even in https mode.

# Notes to Self

 Heroku Deploy

    sbt clean compile
    git push heroku develop:master  # to push (local or server) develop to heroku

Replacing develop with whatever local branch you want to push to Heroku master. 
That will automatically deploy on Heroku, faster than slug deploys.

Domain names mapped:
    web.ripplewaves.com
    connect.ripplewaves.com
    
    https://web.ripplewaves.com/  should get you there with a Certificate Mismatch unfortunately.
    

## Local Dev Hassles

https://localhost:9443/ and session works. Hooray.
Note: Play Session will not work unless using HTTPS
Deploy to Heroku and       https://web.ripplewaves.com/about  works find but http doesnt.
So,   have to setup HTTPS to even do basic development. This is in runmode dev :-( 
    sbt "run -Dhttps.port=9443 -Dhttp.port=8081"     will start on 9000 and 9443
    
