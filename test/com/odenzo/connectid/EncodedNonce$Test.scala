package com.odenzo.connectid

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{ FunSuite, Matchers }

class EncodedNonce$Test extends FunSuite with LazyLogging with Matchers {

  test("EncodedNonce") {
    logger.info("Running...")
    val x = EncodedNonce("AAA", "BBB")
    EncodedNonce("aaaa", "xxxxx").toString shouldEqual "aaaa|xxxxx"
  }

}
