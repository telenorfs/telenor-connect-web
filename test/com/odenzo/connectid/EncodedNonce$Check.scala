package com.odenzo.connectid

import org.scalacheck.Prop.forAll
import org.scalacheck.{ Gen, Properties }

object EncodedNonce$Check extends Properties("EncodedNonce") {

  // This should have precondition a doesn't contain |
  // Can make a special String gen a or precondition

  val validOpGen = Gen.alphaStr.suchThat(_.contains(EncodedNonce.delimeter) == false)

  property("startsWith") = forAll { (op: String, nonce: String) ⇒
    //!a.contains(EncodedNonce.delimeter) ==>
    // TODO: Use correct op generator or take into account
    // require exception somehow

    val rt = EncodedNonce.parse(EncodedNonce(op, nonce).toString).get
    rt.op == op && rt.nonce == nonce
  }

  property("length") = forAll { (a: String, b: String) ⇒
    EncodedNonce(a, b).toString.length == (a.length + b.length + 1)
  }
}
